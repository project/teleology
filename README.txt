Description:
------------
This is the Teleology theme.

Authors:
--------
It was designed by Denis Polevoi for the http://teleology.ru web site.

Known Issues:
-------------
A lot. It is the raw version of this theme. It need to and will be improved.